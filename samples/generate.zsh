#!/bin/zsh
typeset -gr VIMREPODIR=~/a.a/Proj/c/vim
typeset -gr VIM=$VIMREPODIR/src/vim
typeset -ga GVIM
GVIM=( $VIM -g -f )
typeset -gar GVIM
typeset -xr REPO=$0:A:h:h
typeset -xr SAMPLESDIR=$REPO/samples
typeset -gr VIMRC=$SAMPLESDIR/vimrc
typeset -xr RTP=$SAMPLESDIR/rtp
typeset -xr LANG=en_US.UTF-8
typeset -xr FRAWORDIR=$REPO:h/frawor
typeset -gr TOOLSDIR=$REPO:h/repository
typeset -xr OUTDIR=$SAMPLESDIR/out

typeset -gi MOUNT=1
typeset -gi POST=1
typeset -gi FOREGROUND=0
typeset -ga DIRS

typeset -g  list=$'\n\nExamples:\n'
typeset -g  head
typeset -ga vimargs
typeset -g  idetails readmeheader readmefooter readme summary
typeset -gr plname=formatvim

typeset -gr P=$0:t
function help()
{
local s=${P//?/ }
cat <<EOF
    $0 [--nomount] [--nopost] [{dirname}*]
EOF
exit 0
}

[[ $1 == "--help" ]] && help
if [[ $1 == "--nomount" ]] ; then
    MOUNT=0
    shift
fi
typeset -gir MOUNT
if [[ $1 == "--nopost" ]] ; then
    POST=0
    shift
fi
typeset -gir POST
if [[ $1 == "--foreground" ]] ; then
    FOREGROUND=1
    shift
fi

if (( $# )) ; then
    DIRS=( $@:A )
else
    DIRS=( $SAMPLESDIR/*(/) )
fi
typeset -gar DIRS

function vim()
{
    emulate -L zsh
    local -xr VIMRUNTIME=$RTP
    command vim -u $VIMRC $@
}

function gvim()
{
    emulate -L zsh
    local -xr VIMRUNTIME=$RTP
    $GVIM -u $VIMRC $@
}

list+="Documentation: http://vimpluginloader.sourceforge.net/doc/format.txt.html. Script used to format documentation: https://sourceforge.net/p/vimpluginloader/dev-tools/ci/$(hg -R $TOOLSDIR log --template {node} -r .)/tree/format-help.zsh."$'\n'

setopt nullglob

(( MOUNT )) && \
    sshfs -o nonempty zyxsf,formatvim@web.sourceforge.net:/home/groups/f/fo/formatvim/htdocs/samples $OUTDIR
rm -f $OUTDIR/*.html

for directory in $DIRS ; do
    test -x $directory/generate.zsh || continue
    head=$directory:t
    vimargs=( -c 'Format format html to $OUTDIR/'$head'.html' )
    if (( FOREGROUND )) ; then
        vimargs+=( -c 'let g:format_Debugging=1' -c 'let g:format_Debugging_FuncF="/home/zyx/tmp/cformat.vim"' $vimargs )
    else
        vimargs+=( -c 'qa!' )
    fi
    (
        pushd $directory
        . $directory/generate.zsh
        popd
    )
    list+="$(cat $directory/description): http://formatvim.sourceforge.net/samples/$head.html."$'\n'
done

(( MOUNT )) && \
    fusermount -u $OUTDIR

list+=$'\n'

readme="$($TOOLSDIR/pkgdo.pl print-script-readme formatvim)"

readme="${readme/$'\n\n\n'/$list}"

summary="$($TOOLSDIR/pkgdo.pl print-summary formatvim)"

idetails="
You can install this plugin using vim-addon-manager (vimscript #2905) plugin (install vim-addon-manager, run “ActivateAddons formatvim” and add “call vam#ActivateAddons(['$plname'])” to your .vimrc. Vim-addon-manager will install all dependencies for you). You can also use archives attached to this page, they contain plugin with all dependencies, so you do not need to fetch them yourself.

Gentoo users can use the following mercurial overlay: http://hg.code.sf.net/p/vimpluginloader/pluginloader-overlay.
"

(( POST )) && \
    $TOOLSDIR/pkgdo.pl set-vimorg formatvim $summary $readme $idetails
