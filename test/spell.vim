" Test plan:
" + Just bad unhighlighted word
" + Bad highlighted word:
"   + Bad word highlighted as a whole.
"   + Bad word highlighted inside of itself.
"   + Bad word with first few characters highlighted.
"   + Bad word with last few characters highlighted.
"   + Bad word with intersected highlighting.
"
"   * SpellBad highlights bg, syntax fg.
"   * SpellBad highlights bg, syntax bg.
"   * SpellBad highlights bg, syntax attr.
"   * SpellBad highlights bg, syntax bgfg.
"   * SpellBad highlights bg, syntax bgfgattr.
" + Bad highlighted word with matches:
"   + Bad word highlighted as a whole.
"   + Bad word highlighted inside of itself.
"   + Bad word with first few characters highlighted.
"   + Bad word with last few characters highlighted.
"   + Bad word with intersected highlighting.
" + Bad highlighted word with matches and syntax:
"   + Bad word highlighted as a whole by both.
" + Bad highlighted word with matches and syntax and signs and cursor.

source hldefs.vim

view spell

set spell

syntax match BGYellow /[[:digit:]yse+]*xxbgxx[[:digit:]yse+]*/
syntax match FGYellow /[[:digit:]yse+]*xxfgxx[[:digit:]yse+]*/
syntax match Bold     /[[:digit:]yse+]*xxboldxx[[:digit:]yse+]*/
syntax match FYBB     /[[:digit:]yse+]*xxfybbxx[[:digit:]yse+]*/
syntax match FGBB     /[[:digit:]yse+]*xxfgbbxx[[:digit:]yse+]*/

syntax match BGYellow /!\+/
syntax match FGYellow /\$\+/
syntax match Bold     /%\+/
syntax match FYBB     /\^\+/
syntax match FGBB     /&\+/

call matchadd('BGYellow', '[s#]1.\{-}[e#]1')
call matchadd('FGYellow', '[s#]2.\{-}[e#]2')
call matchadd('Bold',     '[s#]3.\{-}[e#]3')
call matchadd('FYBB',     '[s#]4.\{-}[e#]4')
call matchadd('FGBB',     '[s#]5.\{-}[e#]5')
call matchadd('Cleared',  '[s#]6.\{-}[e#]6')

sign define BGGreen linehl=BGGreen
sign place 1 line=32 name=BGGreen buffer=1
