Hi BGRed    -      Red
Hi BGGreen  -      Green
Hi BGBlue   -      Blue
Hi BGYellow -      Yellow
Hi FGRed    Red    -      bold
Hi FGGreen  Green  -      bold
Hi FGBlue   Blue   -      bold
Hi FGYellow Yellow -      bold
Hi FRBG     Red    Green  bold
Hi FGBB     Green  Blue   bold
Hi Bold     -      -      bold
Hi FYBB     Yellow Blue

highlight clear Cleared

fu StrMatch(m)
    return call('printf', ['%-9s(%2u, %2u)..(%2u, %2u) %3i (%2u): %s', a:m.group]+a:m.startpos+a:m.endpos+[a:m.priority, a:m.num, string(a:m.match)])
endfu
fu F60(s)
    return a:s.repeat(" ", 60-strdisplaywidth(a:s))
endfu
fu Ematch(m)
    echom StrMatch(a:m)
endfu
fu PrintMatches(matches)
    for m in a:matches
        call Ematch(m)
    endfor
endfu
