source hldefs.vim
let g:format_UseStyleNames=1
view eolmatches
for args in [
            \['BGRed',    'abc\ndef', 0],
            \['BGYellow', 'b\@=',     0],
            \['BGYellow', 'e\@=',     1],
            \['BGGreen',  '\%1l$',    1],
            \['BGBlue',   '\%1l$',   -1],
            \['BGYellow', '\%1l$',    0],
            \['BGBlue',   '\ng',      0],
            \['BGGreen',  'i$',       0],
            \['BGYellow', 'l\nm',     0],
            \['BGRed',    'o\n',      0],
            \['BGGreen',  'r\ns\@=',  0],
            \
            \['BGRed',    'C\n\nD',   0],
            \['BGRed',    'F\n\nG',   0],
            \['BGRed',    'I\n\nJ',   0],
            \['BGRed',    'L\n\nM',   0],
            \['BGGreen',  '^\%12l',   1],
            \['BGBlue',   '^\%14l',   0],
            \['BGBlue',   '^\%16l',  -1],
            \['BGYellow', '^\%17l',   1],
            \['BGBlue',   '^\%20l',  -1],
        \]
    call call('matchadd', args)
    let pattern=args[1]
    let diff=char2nr('ａ')-char2nr('a')
    let linediff=28
    let pattern=substitute(substitute(pattern,
                \'\%(\\\%(%\d\+\)\?\)\@<!\a',
                \                  '\=nr2char(char2nr(submatch(0))+diff)', 'g'),
                \'\%(\\%\)\@<=\d\+l\@=', '\=submatch(0)+linediff',         'g')
    call call('matchadd', [args[0], pattern, args[2]])
endfor
