let g:format_UseStyleNames=1
view matches
syntax match Error /alpha/ conceal cchar=α
syntax match PreProc /beta/ conceal cchar=β
set tabstop=4
source hldefs.vim
syntax match BGRed   /KK/   conceal cchar=A
syntax match BGGreen /LL/   conceal
syntax match BGBlue  /MM/   conceal cchar=B
syntax match FGRed   /NN\t/ conceal cchar=C
syntax match FGGreen /OO\t/ conceal
syntax match FGBlue  /PP/   conceal cchar=$
syntax match BGRed   /ЙЙ/   conceal cchar=Ё
syntax match BGGreen /КК/   conceal
syntax match BGBlue  /ЛЛ/   conceal cchar=ё
call matchadd('FGYellow', 'B', 0)
call matchadd('FGBlue',   'B', 0)
/ABCDEFGHIJ\n\|A
call matchadd('FGYellow', 'D', 0)
call matchadd('FGBlue',   'D', 0)
call matchadd('BGRed', '\n\t', -1)
call matchadd('BGGreen', '\n\t\@=', 1)
call matchadd('FGBlue', 'alpha', 2)
syn  match     FGRed     /1234567/
call matchadd('FGGreen',  '23456', 3)
call matchadd('BGYellow',  '345',  2)
call matchadd('FGBlue',     '4',   1)
syn  match     FGYellow  /bcdefgh/
call matchadd('BGRed',    'cdefg', 1)
call matchadd('BGGreen',   'def',  2)
call matchadd('BGBlue',     'e',   3) " Warning: also matches e in beta.
syn  match     BGYellow /βγδεζη/
call matchadd('FGRed',   'γδεζηθ',   1)
call matchadd('FGGreen',  'δεζηθι',  2)
call matchadd('FGBlue',    'εζηθικ', 3)
syn  match     BGYellow /ΒΓΔΕΖΗ/
call matchadd('FGRed',   'ΓΔΕΖΗΘ',   3)
call matchadd('FGGreen',  'ΔΕΖΗΘΙ',  2)
call matchadd('FGBlue',    'ΕΖΗΘΙΚ', 1)
call matchadd('FGYellow', 'бвг', 3)
call matchadd('FGGreen',  'б',   2)
call matchadd('FGBlue',     'г', 1)
call matchadd('FGYellow', 'деё', 1)
call matchadd('FGGreen',  'д',   2)
call matchadd('FGBlue',     'ё', 3)
syn  match     FGRed     /АБ/
call matchadd('FGRed',   '0')
call matchadd('BGRed',   'А')
call matchadd('FRBG',    'α')
syn  match     BGYellow /^\r/
call matchadd('BGYellow', "^\<C-n>")
call matchadd('BGGreen', 'АБ')
syn  match     FRBG        /ВГДЕ/
call matchadd('FGGreen',   'В')
call matchadd('BGBlue',     'Г')
call matchadd('FGBB',        'Д')
call matchadd('FGYellow',       'Ж', 5)
call matchadd('FGBlue',         'Ж', 5)
call matchadd('FGYellow',         'И', 6, 101)
call matchadd('FGBlue',           'И', 6, 100)
call matchadd('BGRed', '   \n  ')
call matchadd('BGGreen', '^  ')
call matchadd('FGGreen', '^  \zs  ')
syn  match     FRBG   /αlρhα/ conceal cchar=Α
call matchadd('BGBlue',  'α\zel')
call matchadd('BGRed',   'h\@<=α')
syn  match     FGGreen /ρ/ contained containedin=FRBG

syn  match     FRBG   /\v(^\ {7}\t\ )@<=\ / conceal cchar=A
syn  match     FRBG   /\v(^\ {7}\t)@<=\ /   conceal
syn  match     Normal /\v(^\ {4}\t)@<=\ \ / conceal cchar=Δ

syn  match     BGGreen /^№/
if exists('+re')
   let repref='\%#=1'
else
   let repref=''
endif
execute 'syn  match     FGGreen /'.repref.'\%u0301/'
call          matchadd('BGRed',    repref.'\%u0316')
         syn  match     FGRed   /μ\|ξ/
call          matchadd('BGGreen',  repref.'π\|ς\|σ')
         syn  match     FGBlue  /τ/
sign define FGGreen linehl=FGGreen
sign define BGGreen linehl=BGGreen
sign define FRBG    linehl=FRBG
execute "sign place 1 line=".line('$')." name=FGGreen buffer=1"
command PM :call PrintMatches(g:matches)
set ruler
