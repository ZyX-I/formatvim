let g:format_UseStyleNames=1
view matches
set tabstop=4
source hldefs.vim

call matchaddpos('FGYellow', [1, 2], 0)
call matchaddpos('FGBlue', [2], 0)
call matchaddpos('FGGreen', [3, 11], 0)

call matchaddpos('FGBlue', [[1, 3], [2, 4]], 0)
call matchaddpos('FGYellow', [[2, 4, 3]], 0)

call matchaddpos('FGGreen', [[2, 5, 1]], 0)
call matchaddpos('FGRed', [[3, 1, 10]], 0)
call matchaddpos('FGYellow', [[3, 2, 1]], 0)

call matchaddpos('BGRed', [[4, 1], [9, 2, 2]], 0)
call matchaddpos('BGGreen', [[4, 3], [5, 1], [5, 10]], 0)

call matchaddpos('BGYellow', [[6, col([6, '$'])]])
call matchaddpos('BGYellow', [[8, col([8, '$']) + 1]])
call matchaddpos('BGYellow', [line('$') + 1])
call matchaddpos('BGYellow', [line('$') + 2])
